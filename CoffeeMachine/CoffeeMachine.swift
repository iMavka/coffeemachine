//
//  CoffeeMachine.swift
//  CoffeeMachine
//
//  Created by mavka on 30.08.2018.
//  Copyright © 2018 mavka. All rights reserved.
//

import UIKit

class CoffeeReciept: NSObject {
    var coffee: Int = 0
    var milk: Int = 0
    var water: Int = 0
    var lotok: Int = 0
    
    init (setCoffee: Int, setMilk: Int, setWater: Int, setLotok: Int ) {
        coffee=setCoffee
        milk=setMilk
        water=setWater
        lotok=setLotok
    }
}

class CoffeeMachine: NSObject {
    
    var coffee: Int = 0
    var milk: Int = 0
    var water: Int = 0
    var cap: Int = 0
    var lotok: Int = 0
    
    let maximumCoffee: Int = 50
    let maximumWater: Int = 100
    let maximumMilk: Int = 50
    let maximumCap: Int = 100
    
    private let maxLotok: Int = 30
    private let maxCoffee: Int = 20
    private let maxWater: Int = 20
    private let maxMilk: Int = 10
    private let maxCap: Int = 50
    
    let espresso = CoffeeReciept.init(setCoffee: 1, setMilk: 0, setWater: 1, setLotok: 1)
    let americano = CoffeeReciept.init(setCoffee: 1, setMilk: 0, setWater: 2, setLotok: 1)
    let cappucino = CoffeeReciept.init(setCoffee: 1, setMilk: 1, setWater: 2, setLotok: 1)
    let latte = CoffeeReciept.init(setCoffee: 1, setMilk: 2, setWater: 1, setLotok: 1)
    let double = CoffeeReciept.init(setCoffee: 2, setMilk: 0, setWater: 1, setLotok: 2)
    

    override init() {
        super.init()
        self.addCap()
        self.addMilk()
        self.addWater()
        self.addCoffee()
        self.clearLotok()
        
    }
    
    override var description: String {
        var lotStatus = "full"
        if lotok<maxLotok {
            lotStatus = "not full"
        }
        let result = "Coffee \(coffee); milk \(milk); water \(water); cap \(cap); lotok is \(lotStatus)"
        return result
    }
    
    func makeCoffee (_ reciept: CoffeeReciept) -> Bool {
        var result = true
        if isLotokClear(reciept) && isCoffee(reciept) && isWater(reciept) && isMilk(reciept) && isCap() {
            cap-=1
            coffee-=reciept.coffee
            milk-=reciept.milk
            water-=reciept.water
            lotok+=reciept.lotok
        } else {
            result = false
        }
        return (result)
    }
    
    func isCap () -> Bool {
        if cap == 0 {
            return (false)
        } else {
            return (true)
        }
    }
    
    func isWater (_ reciept: CoffeeReciept) -> Bool {
        if water-reciept.water<0 {
            return (false)
        } else {
            return (true)
        }
    }
    
    func isMilk (_ reciept: CoffeeReciept) -> Bool {
        if  milk-reciept.milk<0 {
            return (false)
        } else {
            return (true)
        }
    }
    
    
    func isCoffee (_ reciept: CoffeeReciept) -> Bool {
        if coffee-reciept.coffee<0 {
            return (false)
        } else {
            return (true)
        }
    }
    
    func isLotokClear (_ reciept: CoffeeReciept) -> Bool {
        if lotok+reciept.lotok<maxLotok {
            return (true)
        } else {
            return (false)
        }
    }
    
    func addCoffee() {
        coffee += maxCoffee
        if coffee > maximumCoffee {
            coffee = maximumCoffee
        }
    }
    
    func addWater() {
        water += maxWater
        if water > maximumWater {
            water = maximumWater
        }
    }
    
    func addMilk() {
        milk += maxMilk
        if milk > maximumMilk {
            milk = maximumMilk
        }
    }
    
    func addCap() {
        cap += maxCap
        if cap > maximumCap {
            cap = maximumCap
        }
    }
    
    func clearLotok() {
        lotok = 0
    }
    
}

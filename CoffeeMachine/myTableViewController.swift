//
//  myTableViewController.swift
//  CoffeeMachine
//
//  Created by mavka on 11.09.2018.
//  Copyright © 2018 mavka. All rights reserved.
//

import UIKit

var myIndex = 0

func log(_ functionName: String = #function, line: Int = #line, file: String = #file, message: String = "") {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "HH:mm:ss"
    print("\(dateFormatter.string(from: Date())) l#:\(line) \(functionName) in \((file as NSString).lastPathComponent) " + message)
}


class myTableViewController: UITableViewController {
    
    var recourceCoffee = "Coffee, Milk, Water, Cap, Clear".components(separatedBy: ", ")
    
    var pricesCoffee = [String(UserDefaults.standard.integer(forKey: priceCoffeeName)),
                        String(UserDefaults.standard.integer(forKey: priceMilkName)),
                        String(UserDefaults.standard.integer(forKey: priceWaterName)),
                        String(UserDefaults.standard.integer(forKey: priceCapName)),
                        String(UserDefaults.standard.integer(forKey: priceClearName))
                        ]
    
    var statusCoffee = [String(myCoffeeMachine.coffee),
                        String(myCoffeeMachine.milk),
                        String(myCoffeeMachine.water),
                        String(myCoffeeMachine.cap),
                        String(myCoffeeMachine.lotok)
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func showAlert (message : String?) {
        let alert = UIStoryboard.init(name: "WinnerStoryboard", bundle: nil).instantiateViewController(withIdentifier: "WinnerID")
        present(alert,animated: true, completion: nil)
    }
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return recourceCoffee.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        cell.textLabel?.text = "\(recourceCoffee[indexPath.row])\t \(statusCoffee[indexPath.row])\t\t                      -\(pricesCoffee[indexPath.row])"
        
        return cell
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        myIndex = indexPath.row
        print("Add resource \(myIndex)")
        var balance = UserDefaults.standard.integer(forKey: balanceName)
        if myIndex == 0 {
            myCoffeeMachine.addCoffee()
            balance -= UserDefaults.standard.integer(forKey: priceCoffeeName)
        }
        if myIndex == 1 {
            myCoffeeMachine.addMilk()
            balance -= UserDefaults.standard.integer(forKey: priceMilkName)
        }
        if myIndex == 2 {
            myCoffeeMachine.addWater()
            balance -= UserDefaults.standard.integer(forKey: priceWaterName)
        }
        if myIndex == 3 {
                myCoffeeMachine.addCap()
            balance -= UserDefaults.standard.integer(forKey: priceCapName)
        }
        if myIndex == 4 {
                myCoffeeMachine.clearLotok()
            balance -= UserDefaults.standard.integer(forKey: priceClearName)
        }
        UserDefaults.standard.set(balance, forKey: balanceName)
        
        UserDefaults.standard.set(myCoffeeMachine.coffee, forKey: currCoffeName)
        UserDefaults.standard.set(myCoffeeMachine.water, forKey: currWaterName)
        UserDefaults.standard.set(myCoffeeMachine.milk, forKey: currMilkName)
        UserDefaults.standard.set(myCoffeeMachine.cap, forKey: currCapName)
        UserDefaults.standard.set(myCoffeeMachine.lotok, forKey: currLotokName)
        
        
        let currStep = UserDefaults.standard.integer(forKey: currStepName) + 1
        UserDefaults.standard.set(currStep, forKey: currStepName)
        
        if balance == UserDefaults.standard.integer(forKey: goalName) {
            // It Is WIN!!
            print ("You are win!!")
            NotificationCenter.default.post(name: winnerNotification,
                                            object: self,
                                            userInfo: nil)
            
        }
        
        print("BALANCE \(balance)  goal \(UserDefaults.standard.integer(forKey: goalName))")
        print(myCoffeeMachine.description)
    }
    
    
}

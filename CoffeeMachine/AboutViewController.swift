//
//  AboutViewController.swift
//  CoffeeMachine
//
//  Created by mavka on 16.09.2018.
//  Copyright © 2018 mavka. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {

    
    @IBOutlet weak var TotalCountLabel: UILabel!
    @IBOutlet weak var WinCountLabel: UILabel!
    @IBOutlet weak var LoosCountLabel: UILabel!
    @IBOutlet weak var MinStepLabel: UILabel!
    @IBOutlet weak var MaxStepLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        TotalCountLabel.text = "Всего сыграно \(String(UserDefaults.standard.integer(forKey: winName) + UserDefaults.standard.integer(forKey: loosName))) игр"
        WinCountLabel.text = "Выиграно \(String(UserDefaults.standard.integer(forKey: winName)))"
        LoosCountLabel.text = "Проиграно \(String(UserDefaults.standard.integer(forKey: loosName)))"
        MinStepLabel.text = "Минимум ходов \(String(UserDefaults.standard.integer(forKey: minStepName)))"
        MaxStepLabel.text = "Максимум ходов \(String(UserDefaults.standard.integer(forKey: maxStepName)))"
        
        
        }

}

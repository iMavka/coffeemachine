//
//  CollectionViewController.swift
//  CoffeeMachine
//
//  Created by 1 on 20.09.2018.
//  Copyright © 2018 web-academy. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class CollectionViewController: UICollectionViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)

        // Do any additional setup after loading the view.
    }

 
}

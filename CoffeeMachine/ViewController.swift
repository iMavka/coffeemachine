//
//  ViewController.swift
//  CoffeeMachine
//
//  Created by mavka on 30.08.2018.
//  Copyright © 2018 mavka. All rights reserved.
//

import UIKit
import AVFoundation

var myCoffeeMachine = CoffeeMachine.init()

let winnerNotification = NSNotification.Name(rawValue: "winnerNotification")


let priceCoffeeName = "priceCoffee"
let priceWaterName = "priceWater"
let priceMilkName = "priceMilk"
let priceCapName = "priceCap"
let priceClearName = "priceClear"
let balanceName = "balance"
let goalName = "goal"

let winName = "winCount"
let loosName = "loosCount"
let minStepName = "minStep"
let maxStepName = "maxStep"
let currStepName = "currStep"

let currCoffeName = "currStatusCoffee"
let currWaterName = "currStatusWater"
let currMilkName = "currStatusMilk"
let currCapName = "currStatusCap"
let currLotokName = "currStatusLotok"

class ViewController: UIViewController {
    
    var objPlayer: AVAudioPlayer?
    
    func playAudioFile() {
        print("in play")
        guard let url = Bundle.main.url(forResource: "SampleAudio_0.4mb", withExtension: "mp3") else { return }
        print ("audio")
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try AVAudioSession.sharedInstance().setActive(true)
            
            // For iOS 11
            objPlayer = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)
            
            // For iOS versions < 11
            objPlayer = try AVAudioPlayer(contentsOf: url)
            
            guard let aPlayer = objPlayer else { return }
            aPlayer.play()
            print ("it was sound")
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    
    var balance = 0
    
    let priceAmericanoName = "priceAmericano"
    let priceEspressoName = "priceEspresso"
    let priceCappuchinoName = "priceCappuchino"
    let priceLatteName = "priceLatte"
    let priceDoubleName = "priceDouble"
    
    
    @IBOutlet weak var ShowCoffeeImage: UIImageView!
    
    
    @IBAction func NewGameButton(_ sender: UIButton) {
        let loosCount = UserDefaults.standard.integer(forKey: loosName) + 1
        UserDefaults.standard.set(loosCount, forKey: loosName)
        
        initGame()
    }
    
    @IBOutlet weak var StatusLabel: UILabel!
    
    @IBAction func MakeCoffeeButton(_ sender: UIButton) {
        var isCoffeeOk = true
        var price = 0
        var balance = UserDefaults.standard.integer(forKey: balanceName)
        let goal = UserDefaults.standard.integer(forKey: goalName)
        
        
        if sender.tag == 10 {
            price = UserDefaults.standard.integer(forKey: priceAmericanoName)
            StatusLabel.text = "Американо. Прибыль +\(price)"
            isCoffeeOk = myCoffeeMachine.makeCoffee(myCoffeeMachine.americano)
            AmericanoButton.titleLabel!.text = "AMERICANO +\(price)"
            let image = UIImage.init(named: "AMERICANO")
            ShowCoffeeImage.image = image
        }
        if sender.tag == 11 {
            price = UserDefaults.standard.integer(forKey: priceCappuchinoName)
            StatusLabel.text = "Капучино. Прибыль +\(price)"
            isCoffeeOk = myCoffeeMachine.makeCoffee(myCoffeeMachine.cappucino)
            CappuchinoButton.titleLabel?.text = "CAPPUCINO +\(String(price))"
            let image = UIImage.init(named: "CAPPUCINO")
            ShowCoffeeImage.image = image
            
        }
        if sender.tag == 12 {
            price = UserDefaults.standard.integer(forKey: priceLatteName)
            StatusLabel.text = "Латте. Прибыль +\(price)"
            isCoffeeOk = myCoffeeMachine.makeCoffee(myCoffeeMachine.latte)
            LatteButton.titleLabel?.text = "LATTE2 +\(String(price))"
            let image = UIImage.init(named: "LATTE")
            ShowCoffeeImage.image = image
            
        }
        
        if sender.tag == 13 {
            price = UserDefaults.standard.integer(forKey: priceEspressoName)
            StatusLabel.text = "Еспрессо. Прибыль +\(price)"
            isCoffeeOk = myCoffeeMachine.makeCoffee(myCoffeeMachine.espresso)
            EspressoButton.titleLabel?.text = "ESPRESSO +\(price)"
            let image = UIImage.init(named: "ESPRESSO")
            ShowCoffeeImage.image = image
            
        }
        if sender.tag == 14 {
            price = UserDefaults.standard.integer(forKey: priceDoubleName)
            StatusLabel.text = "Двойное еспрессо. Прибыль +\(price)"
            isCoffeeOk = myCoffeeMachine.makeCoffee(myCoffeeMachine.double)
            DoubleButton.titleLabel?.text = "DOUBLE +\(price)"
            let image = UIImage.init(named: "AFFOGATO")
            ShowCoffeeImage.image = image
            
        }
        //print (myCoffeeMachine.description)
        print ("balance \(balance)")
        if isCoffeeOk == false {
            
            //  StatusMachine.textColor = UIColor.red
        } else {
            balance += price
            UserDefaults.standard.set(balance, forKey: balanceName)
            BalanceLabel.title = "Баланс \(balance) Цель \(goal)"
            
            let currStep = UserDefaults.standard.integer(forKey: currStepName) + 1
            UserDefaults.standard.set(currStep, forKey: currStepName)
            
            UserDefaults.standard.set(myCoffeeMachine.coffee, forKey: currCoffeName)
            UserDefaults.standard.set(myCoffeeMachine.water, forKey: currWaterName)
            UserDefaults.standard.set(myCoffeeMachine.milk, forKey: currMilkName)
            UserDefaults.standard.set(myCoffeeMachine.cap, forKey: currCapName)
            UserDefaults.standard.set(myCoffeeMachine.lotok, forKey: currLotokName)
            
            //       StatusMachine.textColor = UIColor.darkGray
        }
        if balance == goal {
        
            print("YOU ARE THE WIN!!")
            NotificationCenter.default.post(name: winnerNotification,
                                            object: self,
                                            userInfo: nil)
            winGame()
        }
        // StatusMachine.text = myCoffeeMachine.description
        
    }
    
    @IBOutlet weak var BalanceLabel: UINavigationItem!
    
    
    @IBOutlet weak var EspressoButton: UIButton!
    @IBOutlet weak var AmericanoButton: UIButton!
    @IBOutlet weak var CappuchinoButton: UIButton!
    @IBOutlet weak var LatteButton: UIButton!
    @IBOutlet weak var DoubleButton: UIButton!
    
    
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        playAudioFile()
      
        NotificationCenter.default.addObserver(self, selector: #selector(foo(notification:)),
                                               name: winnerNotification,
                                               object: nil)
        

        let image = UIImage.init(named: "MainIMG")
        ShowCoffeeImage.image = image
        
        if UserDefaults.standard.integer(forKey: currCoffeName) > 0 {
            myCoffeeMachine.coffee = UserDefaults.standard.integer(forKey: currCoffeName)
            myCoffeeMachine.water = UserDefaults.standard.integer(forKey: currWaterName)
            myCoffeeMachine.milk = UserDefaults.standard.integer(forKey: currMilkName)
            myCoffeeMachine.cap = UserDefaults.standard.integer(forKey: currCapName)
            myCoffeeMachine.lotok = UserDefaults.standard.integer(forKey: currLotokName)
        }
        initGame()
        
    
        // Do any additional setup after loading the view, typically from a nib.
        // playCoffeeMachine(100)
    }
    
    @objc func foo(notification: Notification) {
        winGame()
    }

    override func viewDidDisappear(_ animated: Bool) {
        let image = UIImage.init(named: "MainIMG")
        ShowCoffeeImage.image = image
   //     NotificationCenter.default.removeObserver(self)
    }
    
    func winGame () {
        playAudioFile()
        
        let winCount = UserDefaults.standard.integer(forKey: winName) + 1
        UserDefaults.standard.set(winCount, forKey: winName)
        
        let currStep = UserDefaults.standard.integer(forKey: currStepName)
        
        if UserDefaults.standard.integer(forKey: minStepName) == 0 || currStep < UserDefaults.standard.integer(forKey: minStepName) {
            UserDefaults.standard.set(currStep, forKey: minStepName)
        }
        
        if UserDefaults.standard.integer(forKey: maxStepName) < currStep {
            UserDefaults.standard.set(currStep, forKey: maxStepName)
        }
     
        showAlert(message: "")
        initGame()
    }
    
    
    func showAlert (message : String?) {
        let alert = UIStoryboard.init(name: "WinnerStoryboard", bundle: nil).instantiateViewController(withIdentifier: "WinnerID")
        present(alert,animated: true, completion: nil)
    }
    
    
    func initGame () {
        UserDefaults.standard.set(0, forKey: goalName)
        UserDefaults.standard.set(0, forKey: goalName)
        UserDefaults.standard.set(0, forKey: balanceName)
        UserDefaults.standard.set(0, forKey: currStepName)
        
        var goal = UserDefaults.standard.integer(forKey: goalName)
        let balance = UserDefaults.standard.integer(forKey: balanceName)
        if (goal == 0 )
        {
            UserDefaults.standard.set(0, forKey: balanceName)
            
            goal = Int(100 + arc4random()%300)
            UserDefaults.standard.set(goal, forKey: goalName)
            StatusLabel.text = "Ваша Цель - Заработать \(goal) монет"
            
            var priceCoffee = 2 + arc4random()%10
            UserDefaults.standard.set(priceCoffee, forKey: priceAmericanoName)
            AmericanoButton.titleLabel!.text = "AMERICANO +\(priceCoffee)"
            
            priceCoffee = 2 + arc4random()%10
            UserDefaults.standard.set(priceCoffee, forKey: priceEspressoName)
            EspressoButton.titleLabel?.text = "ESPRESSO +\(priceCoffee)"
            
            priceCoffee = 2 + arc4random()%10
            UserDefaults.standard.set(priceCoffee, forKey: priceCappuchinoName)
            CappuchinoButton.titleLabel?.text = "CAPPUCHINO +\(priceCoffee)"
            
            priceCoffee = 2 + arc4random()%10
            UserDefaults.standard.set(priceCoffee, forKey: priceLatteName)
            LatteButton.titleLabel?.text = "LATTE +\(priceCoffee)"
            
            priceCoffee = 2 + arc4random()%10
            UserDefaults.standard.set(priceCoffee, forKey: priceDoubleName)
            DoubleButton.titleLabel?.text = "DOUBLE +\(priceCoffee)"
            
            priceCoffee = 10 + arc4random()%20
            UserDefaults.standard.set(priceCoffee, forKey: priceCoffeeName)
            
            priceCoffee = 10 + arc4random()%10
            UserDefaults.standard.set(priceCoffee, forKey: priceMilkName)
            
            priceCoffee = 5 + arc4random()%10
            UserDefaults.standard.set(priceCoffee, forKey: priceWaterName)
            
            priceCoffee = 10 + arc4random()%10
            UserDefaults.standard.set(priceCoffee, forKey: priceCapName)
            
            priceCoffee = arc4random()%10
            UserDefaults.standard.set(priceCoffee, forKey: priceClearName)
            
        }
        myCoffeeMachine = CoffeeMachine.init()
        BalanceLabel.title = "Баланс 0 Цель \(goal)"
        
    }
    
    func playCoffeeMachine (_ howManyCaps: Int) {
        let myCoffeeMachine = CoffeeMachine.init()
        let myCoffee :[Int: CoffeeReciept] = [0: myCoffeeMachine.americano, 1: myCoffeeMachine.espresso,
                                              2: myCoffeeMachine.cappucino, 3: myCoffeeMachine.latte,
                                              4: myCoffeeMachine.double]
        let myCoffeeName :[Int: String] = [0: "americano", 1: "espresso", 2: "capuccino", 3: "latte", 4: "double"]
        for _ in 0..<howManyCaps {
            let whatCoffee = Int(arc4random()%5)
            var result = myCoffeeMachine.makeCoffee(myCoffee[whatCoffee]!)
            if result == true {
                print ("Take your \(myCoffeeName[whatCoffee]!)")
            } else {
                print ("Something wrong. Oh, wait")
                if (!myCoffeeMachine.isMilk(myCoffee[whatCoffee]!)) {
                    print ("we need milk. Added!")
                    myCoffeeMachine.addMilk()
                }
                if (!myCoffeeMachine.isWater(myCoffee[whatCoffee]!)) {
                    print ("we need water. Added!")
                    myCoffeeMachine.addWater()
                }
                if (!myCoffeeMachine.isCoffee(myCoffee[whatCoffee]!)) {
                    print ("we need coffee. Added!")
                    myCoffeeMachine.addCoffee()
                }
                if (!myCoffeeMachine.isCap()) {
                    print ("we need caps. Added!")
                    myCoffeeMachine.addCap()
                }
                if (!myCoffeeMachine.isLotokClear(myCoffee[whatCoffee]!)) {
                    print ("Lotok is Full. Cleared!")
                    myCoffeeMachine.clearLotok()
                }
                result = myCoffeeMachine.makeCoffee(myCoffee[whatCoffee]!)
            }
        }
        
    }
    
}

